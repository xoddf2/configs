" ~/.vim/vimrc

" Plug
call plug#begin('~/.vim/plugged')
	Plug 'junegunn/vim-plug'
	Plug 'tpope/vim-surround'
	Plug 'ap/vim-css-color'
call plug#end()

" Enable syntax highlighting
syntax on

" Enable filetype detection
filetype plugin indent on

" Improve the % key
runtime macros/matchit.vim

" More visual feedback and no splash screen
set shortmess+=I
set showcmd
set ruler

" Buffers
set hidden

" Split correctly
set splitbelow
set splitright

" Per-file settings
set modeline

" Stop leaving droppings in the current working directory
if !isdirectory($HOME."/.local/var/tmp/vim/swap")
	call mkdir($HOME."/.local/var/tmp/vim/swap", "p", 0700)
endif
if !isdirectory($HOME."/.local/var/tmp/vim/backup")
	call mkdir($HOME."/.local/var/tmp/vim/backup", "p", 0700)
endif

set directory=$HOME/.local/var/tmp/vim/swap
set backupdir=$HOME/.local/var/tmp/vim/backup

" We should take ~/.viminfo and push it somewhere else!
set viminfofile=$HOME/.vim/viminfo

" Fix tab completion
set wildmenu
set wildmode=longest,list

" Search
set incsearch
set hlsearch
set smartcase

" Highlight margin
set colorcolumn=80

" Jump to matching bracket briefly
set showmatch

" Spelling
set spell spelllang=en

" Speed up mode change
set timeoutlen=1000 ttimeoutlen=10

" Do not display title except in gVim
if !has("gui_running")
	set notitle
endif

" Mouse support
set mouse=a

" man page viewing
runtime ftplugin/man.vim

" Keybindings
map ZQ <Nop>

map <Leader>bp :bprevious<CR>
map <Leader>bn :bnext<CR>
map <Leader>ba :ball<CR>
map <Leader>bd :bdelete<CR>
map <Leader>bl :blast<CR>

map <Leader>tn :tabnew<CR>
map <Leader>tc :tabclose<CR>
map <Leader>to :tabonly<CR>

" Truecolor support
if &term =~# '^screen' || &term =~# '^tmux'
	let &t_8f = "\<Esc>[38:2:%lu:%lu:%lum"
	let &t_8b = "\<Esc>[48:2:%lu:%lu:%lum"
endif

if has("termguicolors") && &t_Co >= 256
	set termguicolors
endif

" Use murphy in a >= 256-colour terminal
if !has("gui_running") && &t_Co >= 256
	colorscheme murphy
else
	colorscheme default
	set background=dark
endif
