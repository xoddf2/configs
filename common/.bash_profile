#!/usr/bin/env bash
# .bash_profile

# Set PATH
PATH="/usr/local/games:/usr/games:/usr/local/sbin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin"

if [ -d "$HOME/.local/bin" ]; then
	PATH="$HOME/.local/bin:$PATH"
elif [ -d "$HOME/bin" ]; then
	PATH="$HOME/bin:$PATH"
fi

export PATH

# Set locale
export LANG="en_US.UTF-8"
export LESSCHARSET="utf-8"

# Set editors
EDITOR="ed"

if command -v vim >/dev/null 2>&1; then
	VISUAL="vim"
else
	VISUAL="vi"
fi

export EDITOR VISUAL

# man(1) settings
if [ -d "$HOME/.local/share/man" ]; then
	MANPATH="$HOME/.local/share/man:$MANPATH"
fi

MANWIDTH=78

export MANPATH MANWIDTH

# Sort dotfiles first
export LC_COLLATE="C"

# Perl
PATH="$HOME/perl5/bin:$PATH"
MANPATH="$HOME/perl5/man:$MANPATH"
PERL5LIB="$HOME/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"
PERL_LOCAL_LIB_ROOT="$HOME/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"
PERL_MB_OPT="--install_base \"$HOME/perl5\""
PERL_MM_OPT="INSTALL_BASE=$HOME/perl5"

export PATH MANPATH PERL5LIB PERL_LOCAL_LIB_ROOT PERL_MB_OPT PERL_MM_OPT

# Display a fortune
if command -v fortune >/dev/null 2>&1 && [[ $- == *i* ]]; then
	fortune -s fortunes
	echo
fi

# Source ~/.bashrc if it exists
if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
fi
