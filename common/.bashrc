#!/usr/bin/env bash
# .bashrc

# Check for an interactive shell
[[ $- != *i* ]] && return

# Settings #####################################################################

# Globbing
shopt -s extglob
shopt -s nocaseglob
shopt -s dotglob

# Other
set -o notify
set bell-style none
shopt -s checkwinsize
shopt -s no_empty_cmd_completion

# History ######################################################################

[ "$HOSTNAME" = "blackbox.local" ] && HISTFILE="$HOME/.cache/bash_history"

HISTSIZE=100000
HISTFILESIZE=100000

shopt -s histappend
shopt -s cmdhist

# Prompt #######################################################################

set_prompt()
{
	# Different prompt colour per box
	case "$HOSTNAME" in
		"blackbox.local")
			local hostname_color="$(tput bold; tput setaf 3)" ;;
		*)
			local hostname_color="$(tput bold)" ;;
	esac

	local exit_status='$?'

	# Additional colours
	local pwd_color="$(tput bold; tput setaf 2)"
	local exit_color="$(tput bold; tput setaf 1)"
	local prompt_color="$(tput bold; tput setaf 4)"

	local reset="$(tput sgr0)"

	PS1="\[$hostname_color\]\h\[$reset\] \[$pwd_color\]\w\[$reset\] \[$exit_color\]$exit_status\[$reset\] \[$prompt_color\]\$\[$reset\] "
}

PROMPT_COMMAND="history -a; history -n; set_prompt"

# Aliases ######################################################################

# ls and grep
if [[ $OSTYPE =~ gnu ]]; then
	alias ls="ls --color=auto -lh --group-directories-first"
	alias la="\ls --color=auto -lah --group-directories-first"
	alias grep="grep --color=auto"
else
	alias ls="ls -lh"
	alias la="\ls -lah"
fi

# ps
alias psa="ps auxww"

if [ "$OSTYPE" = "linux-gnu" ]; then
	alias psr="ps auxww --sort=%cpu"
	alias psm="ps auxww --sort=rss"
	alias psx="ps awwfux"
	alias psc="ps wwu -C"
fi

# Other
alias vi="$VISUAL"
alias mem="free -h"
alias df="df -hT"
alias p3="ping -c 3"

# tmux
if command -v tmux >/dev/null 2>&1; then
	alias ta="tmux attach-session"
	alias tad="tmux attach-session -d"
	alias tls="tmux list-sessions"
	alias tlw="tmux list-windows"
fi

# ix.io pastebin
alias ix="curl -n -F 'f:1=<-' http://ix.io"

# mpv (Long ago, this pointed to MPlayer.)
if command -v mpv >/dev/null 2>&1; then
	alias mp="mpv"
	alias mp-monaural="mpv --audio-channels=1"
	alias mp-overscan="mpv --vf=scale=320:240,crop=288:216 --video-aspect=4:3"
fi

# Avoid accidentally exiting main shell in tmux
if [ -n "$TMUX" ] && [ "$TMUX_PANE" = "%0" ]; then
	set -o ignoreeof
	alias exit="false"
	alias logout="false"
fi
